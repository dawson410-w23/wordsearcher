using WordSearching;
using WordSearching.TextSources;

namespace WordSearcherUnitTests;

[TestClass]
public class UnitTest1
{
    ///Please take a look at the test cases below. You'll note there's a LOT of duplicated code.
    ///Concerns about code quality are a little different when it comes to test cases - duplicated code
    ///is much less of an issue. If we have a very similar setup step for all tests we can add a setup method
    ///but for something like this, that's not really necessary.

    ///You'll note there's a LOT of code here, and you might think this is a tremendous effort. This is the result
    /// of about 15 minutes or so of work. The nice thing
    ///about test cases is that because the logic of repeatedly calling your method in different ways is so 
    ///similar, you can pretty easily copy and paste your code with slight alterations to get very high code
    ///coverage without needing to think TOO hard about it.

    ///If you can think of a case that differs slightly or might have some weird interactions, add it in! Try
    ///to avoid redundancies where possible, but if you think that there's something about a test case that makes it
    ///notable, add it in!

    ///TL;DR: Don't worry as much about code duplication and copy pasting with tests. 
    ///If it means more thorough testing, copy paste away!

    //Search Tests
    [TestMethod]
    public void TestSearchNoOccurrences()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        string keyword = "party time!";
        int expectedOcurrences = 0;

        //Act
        int occurrences = searcher.Search(keyword);
        
        //Assert
        Assert.AreEqual(occurrences,expectedOcurrences);

    }

    [TestMethod]
    public void TestSearchOneOccurrence()
    {

        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        string keyword = "test";
        int expectedOcurrences = 1;

        //Act
        int occurrences = searcher.Search(keyword);
        
        //Assert
        Assert.AreEqual(occurrences,expectedOcurrences);

    }

    [TestMethod]
    public void TestSearchOneOccurrenceAtStart()
    {

        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        string keyword = "This";
        int expectedOcurrences = 1;

        //Act
        int occurrences = searcher.Search(keyword);
        
        //Assert
        Assert.AreEqual(occurrences,expectedOcurrences);

    }

    [TestMethod]
    public void TestSearchOneOccurrenceAtEnd()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        string keyword = "found";
        int expectedOcurrences = 1;

        //Act
        int occurrences = searcher.Search(keyword);
        
        //Assert
        Assert.AreEqual(occurrences,expectedOcurrences);
    }

    
    [TestMethod]
    public void TestSearchSeveralOccurrences()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        string keyword = "a";
        int expectedOcurrences = 3;

        //Act
        int occurrences = searcher.Search(keyword);
        
        //Assert
        Assert.AreEqual(occurrences,expectedOcurrences);
    }

    [TestMethod]
    [ExpectedException(typeof(System.IndexOutOfRangeException))]
    public void TestSearchEmptyKeyword()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        string keyword = "";

        //Act
        int occurrences = searcher.Search(keyword);

        //Assert - Exception should have been thrown

    }

    [TestMethod]
    public void TestSearchEmptyText()
    {
        //Arrange
        ITextSource source = new MockTextSource("");
        WordSearcher searcher = new WordSearcher(source);
        string keyword = "keyword";
        int expectedOcurrences = 0;

        //Act
        int occurrences = searcher.Search(keyword);
        
        //Assert
        Assert.AreEqual(occurrences,expectedOcurrences);
    }


    //FindOccurrences Tests
    [TestMethod]
    public void TestFindOccurrencesOneOccurrence()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        searcher.Search("test");
        string expectedOutput = "mple string to test whether occurr";

        //Act
        string output = searcher.FindOccurence(0);
        
        //Assert
        Assert.AreEqual(output,expectedOutput);
    }

    [TestMethod]
    public void TestFindOccurrencesOccurrenceNearStart()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        searcher.Search("is");
        string expectedOutput = "This is a sample st";

        //Act
        string output = searcher.FindOccurence(0);
        
        //Assert
        Assert.AreEqual(output,expectedOutput);
    }

    [TestMethod]
    public void TestFindOccurrencesOccurrenceNearEnd()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        searcher.Search("are");
        string expectedOutput = "er occurrences are found";

        //Act
        string output = searcher.FindOccurence(0);
        
        //Assert
        Assert.AreEqual(output,expectedOutput);
    }

    [TestMethod]
    public void TestFindOccurrencesShortString()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        searcher.Search("a");
        string expectedOutput = "er occurrences are found";

        //Act
        string output = searcher.FindOccurence(2);
        
        //Assert
        Assert.AreEqual(output,expectedOutput);
    }

        [TestMethod]
    public void TestFindOccurrencesSeveralOccurrences()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a string");
        WordSearcher searcher = new WordSearcher(source);
        searcher.Search("a");
        string expectedOutput = "This is a string";

        //Act
        string output = searcher.FindOccurence(0);
        
        //Assert
        Assert.AreEqual(output,expectedOutput);
    }

    [TestMethod]
    [ExpectedException(typeof(System.IndexOutOfRangeException))]
    public void TestFindOccurrencesKeywordNotSet()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a string");
        WordSearcher searcher = new WordSearcher(source);
        

        //Act
        string output = searcher.FindOccurence(0);
        
        //Assert - Will throw exception
    }

    [TestMethod]
    [ExpectedException(typeof(System.ArgumentException))]
    public void TestFindOccurrencesEmptyTextSource()
    {
        //Arrange
        ITextSource source = new MockTextSource("");
        WordSearcher searcher = new WordSearcher(source);

        //Act
        string output = searcher.FindOccurence(0);
        
        //Assert - Will thro ArgumentException
    }


    //The indexer is doing exactly the same thing as FindOccurrences, so I'm going to stick to just a
    //basic test case here to ensure it works the same way. Our tests for findoccurrences should catch other
    //failures.

    [TestMethod]
    public void TestIndexer()
    {
        //Arrange
        ITextSource source = new MockTextSource("This is a sample string to test whether occurrences are found");
        WordSearcher searcher = new WordSearcher(source);
        searcher.Search("test");
        string expectedOutput = "mple string to test whether occurr";

        //Act
        string output = searcher[0];
        
        //Assert
        Assert.AreEqual(output,expectedOutput);
    }

}