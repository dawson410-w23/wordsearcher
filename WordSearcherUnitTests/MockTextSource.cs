namespace WordSearching.TextSources;

public class MockTextSource : ITextSource
{
    private String Text { get; set; }
    public MockTextSource(String text)
    {
        Text = text;
    }
    public string ReadText()
    {
        return Text;
    }
}