using WordSearching.TextSources;

namespace WordSearching;

public class WordSearcher
{
    private const int KeywordNotFound = -1;
    private ITextSource _source { get; set; }
    private String _keyword { get; set; }
    public WordSearcher(ITextSource source)
    {
        _source = source;
        _keyword = "";
    }

    /// <summary>
    /// There are a lot of ways this problem can be solved. The way I'll be solving it is by 
    /// having a helper method that moves from the index of one occurrence to the next one.
    ///
    /// In effect, what I'm really doing is re-implementing the string.IndexOf method, which
    /// would be the cleaner and easier way of solving the problem. But if you didn't realize
    /// that method was available and tried to implement a solution yourself, this is an example
    /// of what that might look like.
    /// </summary>
    public int NextOccurrence(int start)
    {
        string text = _source.ReadText();
        for (int i = start; i < text.Length; i++)
        {
            //If the character we're looking at matches the first character of our keyword, 
            //then we check to see if the whole keyword matches at this position
            if (text[i] == _keyword[0])
            {

                //We take a substring of the same length as our keyword and check if it matches.
                if (text.Substring(i, _keyword.Length).Equals(_keyword))
                {
                    //If so, the index is valid and we return it!
                    return i;
                }
            }
        }
        //If we make it to the end of the loop without finding any occurences, 
        //there must be none left so we return -1.
        return KeywordNotFound;
    }

    public int Search(String keyword)
    {
        _keyword = keyword;
        int nextIndex = NextOccurrence(0);
        int occurences = 0;
        while (nextIndex != KeywordNotFound)
        {
            occurences++;
            //Add 1 when looking for the next index to make sure we don't just keep finding the same one
            nextIndex = NextOccurrence(nextIndex + 1);
        }
        return occurences;
    }

    public string FindOccurence(int occurrence)
    {
        string text = _source.ReadText();
        int occurenceIndex = -1;
        for (int i = 0; i < occurrence+1; i++) //Add 1 because occurence is 0-indexed
        {
            occurenceIndex++;
            occurenceIndex = NextOccurrence(occurenceIndex);
        }

        if (occurenceIndex == KeywordNotFound)
        {
            //if the occurrence does exist throw an exception
            throw new ArgumentException("Occurrence does not exist");
        }

        int startIndex = occurenceIndex - 15;
        if (startIndex < 0)
        {
            startIndex = 0;
        }
        int endIndex = occurenceIndex + 15 + _keyword.Length;
        if (endIndex > text.Length)
        {
            endIndex = text.Length;
        }
        return text.Substring(startIndex, endIndex - startIndex);

    }

    public String this[int index]
    {
        get { return FindOccurence(index); }
    }

}
