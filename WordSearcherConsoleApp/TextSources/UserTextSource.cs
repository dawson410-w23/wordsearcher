namespace WordSearching.TextSources;


public class UserTextSource : ITextSource
    {
        private String text;

        public UserTextSource()
        {
            string? input;
            
            do{
                Console.WriteLine("Please enter your text:");
                input = Console.ReadLine();
            }while(string.IsNullOrEmpty(input));

            text = input;
        }
        public String ReadText()
        {
            return text;
        }
    }