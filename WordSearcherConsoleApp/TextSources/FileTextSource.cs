namespace WordSearching.TextSources;

public class FileTextSource : ITextSource
    {
        private string Path{get;set;}
        public FileTextSource(String path)
        {
            Path = path;
        }

        public string ReadText()
        {
            if(!File.Exists(Path))
            {
                throw new IOException("File cannot be found at the path indicated.");
            }

            string[] lines = File.ReadAllLines(Path);
            string output = "";
            foreach(string line in lines)
            {
                output+= line;

            }
            return output;
        }
    }