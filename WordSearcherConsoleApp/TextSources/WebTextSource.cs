using System.Net;

namespace WordSearching.TextSources;

public class WebTextSource : ITextSource
{
    private string Uri { get; set; }

    public WebTextSource(string uri)
    {
        Uri = uri;
    }

    public string ReadText()
    {
        WebClient myWebClient = new WebClient();
        Stream content = myWebClient.OpenRead(Uri);
        StreamReader reader = new StreamReader(content);
        return reader.ReadToEnd();
    }
}