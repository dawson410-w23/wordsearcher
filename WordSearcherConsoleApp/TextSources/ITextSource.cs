namespace WordSearching.TextSources;

public interface ITextSource
{
    public abstract String ReadText();
}