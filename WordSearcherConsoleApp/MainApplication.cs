﻿using WordSearching.TextSources;

namespace WordSearching;
public class MainApplication
{

    private const int UserSource = 1;
    private const int FileSource = 2;
    private const int WebSource = 3;

    List<ITextSource> sources = new List<ITextSource>();

    public static void Main(String[] args)
    {
        MainApplication runner = new MainApplication();
        Console.Clear();
        Console.WriteLine("Welcome to Word Searcher App");
        runner.GetSources();
        string? keyword;
        do
        {
            do
            {
                Console.WriteLine("Please enter the keyword to search, or no to quit:");
                keyword = Console.ReadLine();
            } while (string.IsNullOrEmpty(keyword));
            runner.SearchKeyword(keyword);

        } while (!keyword.Equals("no"));
        Console.WriteLine("Goodbye");
    }


    private void GetSources()
    {
        int numSources;
        string? input;
        do
        {

            Console.WriteLine("How many sources do you have?");
            input = Console.ReadLine();
        } while (!int.TryParse(input, out numSources));

        for (var i = 0; i < numSources; i++)
        {
            AddSource();
        }

    }

    private void AddSource()
    {
        int sourceType = GetSourceType();

        if (sourceType == UserSource)
        {
            sources.Add(new UserTextSource());
        }
        else if (sourceType == FileSource)
        {
            string? input;
            do
            {
                Console.WriteLine("Please enter the file path:");
                input = Console.ReadLine();
            } while (string.IsNullOrEmpty(input));
            sources.Add(new FileTextSource(input));
        }
        else if (sourceType == WebSource)
        {
            string? input;
            do
            {
                Console.WriteLine("Please enter the web URL:");
                input = Console.ReadLine();
            } while (string.IsNullOrEmpty(input));
            sources.Add(new WebTextSource(input));
        }
    }

    private int GetSourceType()
    {
        string sourceString =
                @"What type of source is it?
                1. User Input
                2. File Source
                3. Web Source";
        bool valid;
        int sourceType;
        do
        {
            Console.WriteLine(sourceString);
            valid = int.TryParse(Console.ReadLine(), out sourceType);
            if (valid)
            {
                valid = (sourceType == UserSource || sourceType == FileSource || sourceType == WebSource);
            }
        } while (!valid);
        return sourceType;

    }


    private void SearchKeyword(string keyword)
    {

        foreach (ITextSource source in sources)
        {
            try
            {
                WordSearcher searcher = new WordSearcher(source);
                int occurrences = searcher.Search(keyword);
                Console.WriteLine($"{occurrences} occurences of {keyword} in Source");
                for (var i = 0; i < occurrences; i++)
                {
                    Console.WriteLine($"Occurence {i} of {keyword}: {searcher[i]}");
                }

            }
            catch (System.Net.WebException)
            {
                Console.WriteLine("Error with web source, webpage inaccessible");
            }
            catch (System.IO.IOException)
            {
                Console.WriteLine($"Error with file source, could not access path");
            }
        }
    }
}