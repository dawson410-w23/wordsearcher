# WordSearcher

This is a solution for the 410 wordsearcher assignment. It is intended to give you an idea as to how aspects of this problem could be solved.

Known issue: This is running on .NET 7.0, not .NET 5.0 like we have on the labs. Future commits will issue and update for this, but if you want to get it working you can modify the .csproj files to use the appropriate version of .NET.